package model;

public class RoomDTO {
	private long id;
    private long creatorId;
    private long adversaryId;
    private long bet;
    private long creatorCardId;
    private long adversaryCardId;

    public RoomDTO() {
    }

    public RoomDTO(long id, long creatorId, long adversaryId, long bet, long creatorCardId,
            long adversaryCardId) {
        this.id = id;
        this.creatorId = creatorId;
        this.adversaryId = adversaryId;
        this.bet = bet;
        this.creatorCardId = creatorCardId;
        this.adversaryCardId = adversaryCardId;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatorId() {
        return this.creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public long getAdversaryId() {
        return this.adversaryId;
    }

    public void setAdversaryId(long adversaryId) {
        this.adversaryId = adversaryId;
    }

    public long getBet() {
        return this.bet;
    }

    public void setBet(long bet) {
        this.bet = bet;
    }

    public long getcreatorCardId() {
        return this.creatorCardId;
    }

    public void setcreatorCardId(long creatorCardId) {
        this.creatorCardId = creatorCardId;
    }

    public long getAdversaryCardId() {
        return this.adversaryCardId;
    }

    public void setAdversaryCardId(long adversaryCardId) {
        this.adversaryCardId = adversaryCardId;
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", creatorId='" + getCreatorId() + "'" +
                ", adversaryId='" + getAdversaryId() + "'" +
                ", bet='" + getBet() + "'" +
                ", creatorCardId='" + getcreatorCardId() + "'" +
                ", adversaryCardId='" + getAdversaryCardId() + "'" +
                "}";
    }
}
