package model;


public class UserDTO {
	private Integer id;
    private String username;
    private String mail;
    private Integer money;

    public UserDTO() {
    }

    public UserDTO(Integer id, String username, String mail, Integer money) {
        super();
        this.id = id;
        this.username = username;
        this.mail = mail;
        this.money = money;
    }

    public Integer getId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getMoney() {
        return this.money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }
    
    @Override
    public String toString() {
    	
		return "{" +
				"\"id\":\"" + getId() + "\"" +
				", \"username\":\"" + getUsername() + "\"" +
				", \"mail\":\"" + getMail() + "\"" +
				", \"money\":" + getMoney() + 
				"}";
    }

}
