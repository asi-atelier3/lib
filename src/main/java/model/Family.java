package model;

public enum Family {
    COMMON,
    SPECIAL,
    RARE,
    EPIC,
    LEGENDARY,
    MYSTICAL
}
